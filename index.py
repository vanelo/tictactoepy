isplayer1turn = True
turn = "player1"
letter = {"player1": "", "player2": ""}
winner = ""
tic = [" ", " ", " ", " ", " ", " ", " ", " ", " "]

def writeintictactoe():
	global isplayer1turn
	global turn
	global letter
	global winner
	global tic
	invalid = True
	while invalid == True:
		position = int(input(f"Please {turn}, enter a valid position [1 - 9]: "))
		if position < 1 or position > 9 or tic[position-1] != " ":
			invalid = True
		else:
			invalid = False
			break

	tic[position-1] = letter[turn]
	print(" {}|{}|{} \n-------\n {}|{}|{} \n-------\n {}|{}|{}  ".format(tic[0], tic[1], tic[2], tic[3], tic[4], tic[5], tic[6], tic[7], tic[8]))

	if isplayer1turn == True:
		turn = "player2"
		isplayer1turn = False
	else:
		turn = "player1"
		isplayer1turn = True

def whoiswinner():
	global isplayer1turn
	global winner
	global tic
	for i in range(0,2):
		if tic[i] != "" and ((tic[i] == tic[i+1] and tic[i+1] == tic[i+2]) or (tic[i] == tic[i+3] and tic[i+3] == tic[i+6])):
			if isplayer1turn == True:
				winner = "player2"
			else:
				winner = "player1"
	if (tic[0] != "" and tic[0] == tic[4] and tic[4] == tic[8]) or (tic[2] != "" and tic[2] == tic[4] and tic[4] == tic[6]):
		if isplayer1turn == True:
			winner = "player2"
		else:
			winner = "player1"

def do_play_again():
	global isplayer1turn
	global turn
	global letter
	global winner
	global tic
	res = input("Do you want to play again? (y/n): ").lower()
	if res == "y":
		# Reset values
		isplayer1turn = True
		turn = "player1"
		letter = {"player1": "", "player2": ""}
		winner = ""
		tic = [" ", " ", " ", " ", " ", " ", " ", " ", " "]
		go_play()

def go_play():
	global letter
	global winner
	global tic
	# intro
	print("Have fun and play tic tac toe with me!")
	print(" 1|2|3 \n-------\n 4|5|6 \n-------\n 7|8|9 ")

	# Choice letter
	letter["player1"] = input("Player1, Choose 'x' or 'o' to start game: ")
	if letter["player1"] != "x":
		letter["player2"] = "x"
	else:
		letter["player2"] = "o"

	# Start Game
	totalletters = ""
	while len(totalletters) < 8:
		totalletters = ""
		for x in range(0,8):
			if tic[x] != " ":
				totalletters += tic[x]
		writeintictactoe()
		if len(totalletters) > 3:
			whoiswinner()
			if len(winner) > 0:
				print(f"Congratulation {winner}!!!")
				break

	if len(totalletters) > 8:
		print("Game over :(")
	do_play_again()

go_play()